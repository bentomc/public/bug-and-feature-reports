# Welcome to the public Bugs and Feature Requests Repository
This is where you can report any bugs or features you would like to be added. Please add the appropriate `label` to your `issue card` so our team can respond to them.
## Issue template

When creating an issue fill in the following information (If you can):

**Description:**
(Should include how we can reproduce the issue. If your bug is not reproducible, then it will never get fixed. How should we implement this feature?)

**Where the bug has been found? (if applicable)**

**Where did the bug happen? (if applicable)**

**Why should we add this feature? (if applicable)**

### You can create a new issue card [here](https://gitlab.com/bentomc/public/bug-and-feature-reports/-/issues)

